package com.treinamento.android.cwicoffee;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.treinamento.android.cwicoffee.adapter.CoffeeDetailsAdapter;
import com.treinamento.android.cwicoffee.model.Coffee.Coffee;
import com.treinamento.android.cwicoffee.repository.CoffeeRepository;
import com.treinamento.android.cwicoffee.utils.Constants;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;
import static com.treinamento.android.cwicoffee.constants.CoffeeRepositoryConstants.COFFEE_TABLE;

public class MainActivity extends BaseActivity implements CoffeeDetailsAdapter.OnItemClickListener {

    private RecyclerView rvCoffees;
    private ArrayList<Coffee> listCoffees;
    private CoffeeDetailsAdapter adapter;
    private Button btnGoToCreateCoffee;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpNavDrawer();
        initializeComponents();
        addbtnGoToCreateCoffeeListener();
    }

    private void initializeComponents() {
        listCoffees = new ArrayList<>();
        btnGoToCreateCoffee = findViewById(R.id.btn_create_coffee);
        rvCoffees = findViewById(R.id.rv_coffee);
        fazerGambiDeUltimaHora();
    }

    @Override
    public void onItemClick(Coffee coffee) {
        Intent intent = new Intent(this, CoffeeDetailsActivity.class);
        intent.putExtra(Constants.COFFEE_EXTRA, coffee);
        startActivity(intent);
    }

    private void populateRecyclerView() {
        rvCoffees.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CoffeeDetailsAdapter(listCoffees, this);
        rvCoffees.setAdapter(adapter);
    }

    private void fazerGambiDeUltimaHora() {
        FirebaseDatabase.getInstance().getReference().child(COFFEE_TABLE).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listCoffees = new ArrayList<>();
                for (DataSnapshot coffeeSnapshot: dataSnapshot.getChildren()) {
                    Coffee coffee = coffeeSnapshot.getValue(Coffee.class);
                    coffee.setKeyCoffee(coffeeSnapshot.getKey());
                    listCoffees.add(coffee);
                }
                populateRecyclerView();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, CoffeeRepository.class.toString() + "getAllCoffees", databaseError.toException());
            }
        });
    }

    private void addbtnGoToCreateCoffeeListener() {
        btnGoToCreateCoffee.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, RegisterCoffeeActivity.class));
                finish();
            }
        });
    }
}