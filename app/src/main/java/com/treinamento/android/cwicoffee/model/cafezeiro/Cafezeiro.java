package com.treinamento.android.cwicoffee.model.cafezeiro;

import com.treinamento.android.cwicoffee.enums.Andar;
import com.treinamento.android.cwicoffee.enums.StatusCafezeiro;
import com.treinamento.android.cwicoffee.enums.TipoCafezeiro;

import java.io.Serializable;

public class Cafezeiro implements Serializable {

    private Long idCafezeiro;
    private String nome;
    private Andar andar;
    private String posicao;
    private StatusCafezeiro status;
    private Double saldo;
    private TipoCafezeiro tpCafezeiro;
    private String email;

    // Necessário construtor vazio pra tretas do firebase
    public Cafezeiro() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getIdCafezeiro() {
        return idCafezeiro;
    }

    public void setIdCafezeiro(Long idCafezeiro) {
        this.idCafezeiro = idCafezeiro;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Andar getAndar() {
        return andar;
    }

    public void setAndar(Andar andar) {
        this.andar = andar;
    }

    public String getPosicao() {
        return posicao;
    }

    public void setPosicao(String posicao) {
        this.posicao = posicao;
    }

    public StatusCafezeiro getStatus() {
        return status;
    }

    public void setStatus(StatusCafezeiro status) {
        this.status = status;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public TipoCafezeiro getTpCafezeiro() {
        return tpCafezeiro;
    }

    public void setTpCafezeiro(TipoCafezeiro tpCafezeiro) {
        this.tpCafezeiro = tpCafezeiro;
    }

}
