package com.treinamento.android.cwicoffee.repository;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.treinamento.android.cwicoffee.model.cafezeiro.Cafezeiro;

import static com.treinamento.android.cwicoffee.constants.CafezeiroRepositoryConstants.CAFEZEIRO_TABLE;

// TODO: Fazer os repositories de uma maneira mais abstrata pra fazer as classes de repository simples
public class CafezeiroRepository {

    private DatabaseReference cafezeiroRepository;

    public CafezeiroRepository() {
        cafezeiroRepository = FirebaseDatabase.getInstance().getReference().child(CAFEZEIRO_TABLE);
    }

    public void createCafezeiro(Cafezeiro cafezeiro) {
        // Com o méotdo push eu crio um "nodo" na tabela
        // esse nodo é filho da referencia que eu peguei no construtor
        // o getKey me traz o hash gerado pra esse nodo, que é basicamenteo  id dele
        String key = cafezeiroRepository.push().getKey();

        // O método child recebe um valor que é basicamente o filtro que tu quer do nodo que a tua
        // referencia tá. Nesse caso nossa referencia está no nodo pai, que representa a tabela
        // `cafezeiros`
        // Com o setValue, insiro o cafezeiro nesse nodo! E é isso S2
        cafezeiroRepository.child(key).setValue(cafezeiro);
    }
}
