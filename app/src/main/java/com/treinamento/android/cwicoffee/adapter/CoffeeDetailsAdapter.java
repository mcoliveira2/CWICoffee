package com.treinamento.android.cwicoffee.adapter;

import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.treinamento.android.cwicoffee.R;
import com.treinamento.android.cwicoffee.model.Coffee.Coffee;

import java.util.ArrayList;

/**
 * Created by Diuly on 05/12/2017.
 */

public class CoffeeDetailsAdapter extends RecyclerView.Adapter<CoffeeDetailsAdapter.ViewHolder> implements View.OnClickListener {

    private ArrayList<Coffee> listCoffee;
    private OnItemClickListener onItemClickListener;

    public CoffeeDetailsAdapter(ArrayList<Coffee> listCoffee, OnItemClickListener onItemClickListener) {
        this.listCoffee = listCoffee;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_active_coffee, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Coffee coffee = listCoffee.get(position);
        holder.tvCoffeeDate.setText(coffee.getData().toString());
        holder.tvCoffeeStatus.setText(coffee.getStatusCoffee().getMessage());
        holder.tvCoffeeAccount.setText(coffee.getValorGasto().toString());
        holder.itemView.setTag(coffee);
        holder.itemView.setOnClickListener(this);
        if(coffee.getListEmailCafezeiro() != null && coffee.getListEmailCafezeiro().contains(FirebaseAuth.getInstance().getCurrentUser().getEmail())) {
            holder.coffeeIcon.setVisibility(View.VISIBLE);
        } else {
            holder.coffeeIcon.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return listCoffee.size();
    }

    @Override
    public void onClick(View v) {
        onItemClickListener.onItemClick((Coffee) v.getTag());
    }

    public interface OnItemClickListener {
        void onItemClick(Coffee coffee);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvCoffeeDate, tvCoffeeStatus, tvCoffeeAccount;
        ImageView coffeeIcon;

        public ViewHolder(View itemView) {
            super(itemView);

            tvCoffeeDate = itemView.findViewById(R.id.tv_coffee_date);
            tvCoffeeStatus = itemView.findViewById(R.id.tv_coffee_status);
            tvCoffeeAccount = itemView.findViewById(R.id.tv_coffee_account);
            coffeeIcon = itemView.findViewById(R.id.icon_coffee);
        }
    }

}
