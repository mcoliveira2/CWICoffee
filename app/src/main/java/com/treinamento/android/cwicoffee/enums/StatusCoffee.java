package com.treinamento.android.cwicoffee.enums;

/**
 * Created by Diuly on 05/12/2017.
 */

public enum StatusCoffee {

    ABERTO("AB", "Aberto"),
    CONCLUIDO("CL", "Concluído"),
    CANCELADO("CA", "Cancelado"),;

    private String valor;
    private String message;

    StatusCoffee(String valor, String message) {
        this.valor = valor;
        this.message = message;
    }

    public String getValor(){
        return this.valor;
    }

    public String getMessage() {
        return message;
    }
}
