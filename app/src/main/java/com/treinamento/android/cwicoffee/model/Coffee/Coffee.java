package com.treinamento.android.cwicoffee.model.Coffee;

import com.treinamento.android.cwicoffee.enums.StatusCoffee;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Diuly on 05/12/2017.
 */

public class Coffee implements Serializable {

    private String keyCoffee;
    private String data;
    private ArrayList<String> listEmailCafezeiro;
    private Double valorGasto;
    private StatusCoffee statusCoffee;

    public Coffee(){}

    public String getKeyCoffee() {
        return keyCoffee;
    }

    public void setKeyCoffee(String keyCoffee) {
        this.keyCoffee = keyCoffee;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public ArrayList<String> getListEmailCafezeiro() {
        return listEmailCafezeiro;
    }

    public void setListEmailCafezeiro(ArrayList<String> listEmailCafezeiro) {
        this.listEmailCafezeiro = listEmailCafezeiro;
    }

    public Double getValorGasto() {
        return valorGasto;
    }

    public void setValorGasto(Double valorGasto) {
        this.valorGasto = valorGasto;
    }

    public StatusCoffee getStatusCoffee() {
        return statusCoffee;
    }

    public void setStatusCoffee(StatusCoffee statusCoffee) {
        this.statusCoffee = statusCoffee;
    }
}
