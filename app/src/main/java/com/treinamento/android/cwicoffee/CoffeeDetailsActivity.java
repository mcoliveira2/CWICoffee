package com.treinamento.android.cwicoffee;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.treinamento.android.cwicoffee.adapter.CoffeeDetailsParticipantsAdapter;
import com.treinamento.android.cwicoffee.model.Coffee.Coffee;
import com.treinamento.android.cwicoffee.repository.CoffeeRepository;
import com.treinamento.android.cwicoffee.utils.Constants;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

import com.treinamento.android.cwicoffee.enums.StatusCoffee;

/**
 * Created by Diuly on 05/12/2017.
 */

public class CoffeeDetailsActivity extends AppCompatActivity {

    private Coffee coffee;
    private Button buttonParticipate;
    private TextView coffeeStatus, coffeeValorGasto, coffeeData;
    private RecyclerView rvParticipantes;
    private ArrayList<String> listEmailCafezeiro;
    private CoffeeDetailsParticipantsAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coffee_details);
        initializeComponents();
        coffee = (Coffee) getIntent().getSerializableExtra(Constants.COFFEE_EXTRA);
        listEmailCafezeiro = coffee.getListEmailCafezeiro() == null ? listEmailCafezeiro = new ArrayList<>() : coffee.getListEmailCafezeiro();
        if (coffee != null) {
            setUpDetails();
            resolveView();
            populateRecyclerView();
        }
    }

    private void initializeComponents() {
        coffeeStatus = findViewById(R.id.tv_coffee_status);
        coffeeValorGasto = findViewById(R.id.tv_coffee_balance);
        coffeeData = findViewById(R.id.tv_date_of_coffee);
        buttonParticipate = findViewById(R.id.btn_participate);
        rvParticipantes = findViewById(R.id.recycler_participation);
        rvParticipantes.setVisibility(View.INVISIBLE);
    }

    private void populateRecyclerView() {
        rvParticipantes.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CoffeeDetailsParticipantsAdapter(listEmailCafezeiro, this);
        rvParticipantes.setAdapter(adapter);
    }

    private void resolveView() {
        if (coffee.getListEmailCafezeiro() != null && coffee.getListEmailCafezeiro().contains(FirebaseAuth.getInstance().getCurrentUser().getEmail())) {
            buttonParticipate.setVisibility(View.INVISIBLE);
            rvParticipantes.setVisibility(View.VISIBLE);

            // Fazer esquemas de recylerview
        } else {
            buttonParticipate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String emailcafezeiro = FirebaseAuth.getInstance().getCurrentUser().getEmail();
                    if (coffee.getListEmailCafezeiro() != null) {
                        listEmailCafezeiro = coffee.getListEmailCafezeiro();
                    } else {
                        coffee.setListEmailCafezeiro(listEmailCafezeiro);
                    }
                    coffee.getListEmailCafezeiro().add(emailcafezeiro);
                    coffee.setValorGasto(coffee.getValorGasto() + 5.00);
                    CoffeeRepository coffeeRepository = new CoffeeRepository();
                    coffeeRepository.updateCoffee(coffee);
                    resolveView();
                    setUpDetails();
                    populateRecyclerView();
                }
            });
        }
    }

    private void setUpDetails() {
        coffeeStatus.setText(resolveCoffeeStatus());
        coffeeValorGasto.setText(resolveValorGasto());
        coffeeData.setText(coffee.getData());
    }

    private String resolveValorGasto() {
        if (coffee.getValorGasto() == null) {
            return "R$00,00";
        }
        return coffee.getValorGasto().toString();
    }

    private String resolveCoffeeStatus() {
        return StatusCoffee.ABERTO.getMessage();
    }
}
