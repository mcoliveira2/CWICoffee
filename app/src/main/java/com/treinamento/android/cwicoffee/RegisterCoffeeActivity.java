package com.treinamento.android.cwicoffee;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.treinamento.android.cwicoffee.enums.StatusCoffee;
import com.treinamento.android.cwicoffee.model.Coffee.Coffee;
import com.treinamento.android.cwicoffee.repository.CoffeeRepository;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static android.text.TextUtils.isEmpty;

/**
 * Created by Diuly on 03/12/2017.
 */

public class RegisterCoffeeActivity extends BaseActivity {

    TextView dateView;
    Button buttonRegisterCafe, inputDate;
    private int mYear, mMonth, mDay;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_coffee);
        setUpNavDrawer();

        initializeComponents();

        addButtonRegisterCafeListener();
    }

    private void addButtonRegisterCafeListener() {
        buttonRegisterCafe.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Coffee coffee = mapViewToCoffee();
                if(coffee == null) {
                    return;
                }
                CoffeeRepository coffeeRepository = new CoffeeRepository();
                coffeeRepository.createCafe(coffee);
                Toast.makeText(RegisterCoffeeActivity.this, "Café registrado com sucesso!", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(RegisterCoffeeActivity.this, MainActivity.class));
                finish();
            }
        });
    }

    private Coffee mapViewToCoffee() {
        String data = dateView.getText().toString();
        if(!isEmpty(data)) {
           Coffee coffee = new Coffee();
           coffee.setData(data);
           coffee.setStatusCoffee(StatusCoffee.ABERTO);
           coffee.setValorGasto(0.0);
           return coffee;
        }

        Toast.makeText(this, "Insira uma data.", Toast.LENGTH_LONG).show();
        return null;
    }

    private void initializeComponents() {
        buttonRegisterCafe = findViewById(R.id.btn_register_coffee);
        dateView = findViewById(R.id.data_cafe_exibicao);

        initializeDatePicker();
    }
    private void initializeDatePicker() {
        inputDate = findViewById(R.id.data_cafe);

        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "dd-MM-yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
                // Texto para visualização da data selecionada
                dateView.setText(sdf.format(myCalendar.getTime()));
            }
        };

        inputDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dpd = new DatePickerDialog(RegisterCoffeeActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // Display Selected dateView in textbox

                                if (year < mYear)
                                    view.updateDate(mYear,mMonth,mDay);

                                if (monthOfYear < mMonth && year == mYear)
                                    view.updateDate(mYear,mMonth,mDay);

                                if (dayOfMonth < mDay && year == mYear && monthOfYear == mMonth)
                                    view.updateDate(mYear,mMonth,mDay);

                                dateView.setText(dayOfMonth + "-"
                                        + (monthOfYear + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);
                dpd.getDatePicker().setMinDate(System.currentTimeMillis());
                dpd.show();
            }
        });
    }
}
