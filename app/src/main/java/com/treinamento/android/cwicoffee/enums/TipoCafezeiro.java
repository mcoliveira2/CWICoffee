package com.treinamento.android.cwicoffee.enums;

/**
 * Created by daniel.figueiredo on 25/11/2017.
 */

public enum TipoCafezeiro {
    ADMIN("ADM"),
    NORMAL("NRM"),;

    private String tipo;

    TipoCafezeiro(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return this.tipo;
    }
}
