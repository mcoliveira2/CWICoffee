package com.treinamento.android.cwicoffee.adapter;

import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.treinamento.android.cwicoffee.CoffeeDetailsActivity;
import com.treinamento.android.cwicoffee.R;
import com.treinamento.android.cwicoffee.model.Coffee.Coffee;
import com.treinamento.android.cwicoffee.model.cafezeiro.Cafezeiro;

import java.util.ArrayList;

public class CoffeeDetailsParticipantsAdapter extends RecyclerView.Adapter<CoffeeDetailsParticipantsAdapter.ViewHolder> {

    private ArrayList<String> listParticipants;

    public CoffeeDetailsParticipantsAdapter(ArrayList<String> listParticipants, CoffeeDetailsActivity coffeeDetailsActivity) {
        this.listParticipants = listParticipants;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_cafezeiros, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String emailCafezeiro = listParticipants.get(position);
        holder.tvParticipantEmail.setText(emailCafezeiro);
    }

    @Override
    public int getItemCount() {
        return listParticipants != null ? listParticipants.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvParticipantEmail;

        public ViewHolder(View itemView) {
            super(itemView);
            tvParticipantEmail = itemView.findViewById(R.id.tv_email_cafezeiro);
        }
    }

}
