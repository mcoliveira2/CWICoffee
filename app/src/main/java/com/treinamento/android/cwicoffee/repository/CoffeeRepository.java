package com.treinamento.android.cwicoffee.repository;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.treinamento.android.cwicoffee.model.Coffee.Coffee;

import static com.treinamento.android.cwicoffee.constants.CoffeeRepositoryConstants.COFFEE_TABLE;

/**
 * Created by Diuly on 05/12/2017.
 */

public class CoffeeRepository {

    private DatabaseReference coffeeRepository;

    public CoffeeRepository() {
        coffeeRepository = FirebaseDatabase.getInstance().getReference().child(COFFEE_TABLE);
    }

    public void createCafe(Coffee coffee) {
        String key = coffeeRepository.push().getKey();
        coffeeRepository.child(key).setValue(coffee);
    }

    public void updateCoffee(Coffee coffee) {
        coffeeRepository.child(coffee.getKeyCoffee()).setValue(coffee);
    }
}
